<?php

namespace OverlayConsulting\Salesforcesdk\Commands;

use Illuminate\Console\Command;
use OverlayConsulting\Salesforcesdk\Models\Field;

class FieldCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pm:fieldcheck';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check every field from PM to be sure it didnt change';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = storage_path()."/app/uploads";
        $files = array_diff(scandir($path), ['.', '..', 'parse.php']);
        $lastFile = $path."/".end($files);
        $xml = simplexml_load_file($lastFile);
        $data = $xml->informationAndMetrics->row;

        $pmFields = (array) $data[0];
        $pmFields = array_keys($pmFields);

        $stored = Field::where('source', 'PortFolio Manager')->get();
        $badFields = [];

        foreach($stored as $storedField) {
            if (!in_array($storedField->label, $pmFields)) {
                $badFields[] = $storedField->lable;
            } else {
                $storedField->status = true;
                $storedField->save();
                $this->info($storedField->label ." is ok");
            }
        }

        if (count($badFields) >0 ) {
            // send an email?
            $fieldsStr = implode(", ", $badFields);
            \Log::info('Found bad PM fields not in local bridge. '.$fieldsStr);
        }

    }
}
