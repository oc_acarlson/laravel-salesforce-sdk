<?php
// do not cache results
ini_set('soap.wsdl_cache_enabled', '0');

return [
    /**
     * Not sure this is needed
     */
    'LOGIN_API' => 'https://login.salesforce.com/services/Soap/u/39.0',
    /**
     * This is the relative path to the WSDL file downloaded from SF.
     */
    'WSDL' => '/app/wsdl.xml',
    /**
     * This is the Username all actions will be made by.
     */
    'USERNAME' => env('SALESFORCE_API_USERNAME'),

        /**
     * This is the password associated with the username 
     */
    'PASSWORD' => env('SALESFORCE_API_PASSWORD'),

    /**
     * this is the API token associated with the username
     */
    'TOKEN' => env('SALESFORCE_API_TOKEN'),
    
    /**
     * enable sf response logging
     */
    'SF_LOGGING' => env('SF_LOGGING', false),
    
    /**
     * set SF LOG FILE, stored in storage/logs
     */
    'SF_LOG_NAME' => env('SF_LOG_PATH', 'sf.log'),
    
    /**
     * Use SF Contacts or a custom table, if true SF Standard object Contact is used
     */
    'USE_SF_CONTACTS' => false,
    
    /**
     * Store the BCC Email Address in config
     */
    'BCC_EMAIL_ADDRESS' => env('BCC_EMAIL_ADDRESS', ''),
];

