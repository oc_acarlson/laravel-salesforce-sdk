<?php

namespace OverlayConsulting\Salesforcesdk\Helpers;

class SFLogger
{   
    /**
     * Write the log file, append only
     * Format is date, time, message
     */
    static function Log($message) {
        if (config('salesforce.SF_LOGGING')) { // only log if set to enabled
            // make sure we have a string and not something that won't log
            if (!is_string($message)) {
                $message = json_encode($message);
            }
            
            $logPath = storage_path('logs')."/".config('salesforce.SF_LOG_NAME');

            $arrMessage = [
                'date' => date('Y-m-d'),
                'time' => date('H:i:s'),
                'message' => $message,
            ];
            
            $fh = fopen($logPath, 'a+');
            fputcsv($fh, $arrMessage, "\t");
            fputcsv($fh, ["\r\n"]);
            fclose($fh);
            return;
        }
    }
}
