<?php
namespace OverlayConsulting\Salesforcesdk\Seeds;

use Illuminate\Database\Seeder;
use OverlayConsulting\Salesforcesdk\Models\Field;

class FieldSeedPM extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path()."/app/uploads";
        $files = array_diff(scandir($path), ['.', '..', 'parse.php']);
        $lastFile = $path."/".end($files);
        $xml = simplexml_load_file($lastFile);
        $data = $xml->informationAndMetrics->row;
        $changed = [];

        $fields = (array) $data[0];
        $fields = array_keys($fields);

        foreach ($fields as $field) {
            $lField = new Field;
            $lField->source = "Portfolio Manager";
            $lField->label = $field;
            $lField->save();
        }
    }

    /**
     * convert a simplexml object into a stdclass object
     *
     * @param simplexml object $xmlData
     */
    public function xml2obj($xmlData)
    {
        $result = new stdclass;

        foreach ($xmlData->children() as $key=>$value) {
            $result->$key = $value->__toString();
        }

        return $result;
    }
}
