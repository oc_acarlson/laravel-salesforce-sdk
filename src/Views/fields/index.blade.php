@extends('layouts.app')

@section('content')
<div class="container">
    <h2>View and manage Fields between PM and Salesforce</h2>
    
    <div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="tabs" role="tablist">
            <li role="presentation" class="active"><a href="#summary" aria-controls="summary" role="tab" data-toggle="tab">Summary</a></li>
            <li role="presentation"><a href="#salesforce" aria-controls="salesforce" role="tab" data-toggle="tab">Salesforce</a></li>
            <li role="presentation"><a href="#pm" aria-controls="pm" role="tab" data-toggle="tab">Portfolio Manager</a></li>
            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="summary">
                <div class="row">
                    <h4>Add Relationship</h4>
                    <div class="col-sm-3">
                        <strong>PM Field</strong><br />
                        <select id="pmFields" class="form-control">
                            <option value=''>Select</option>
                            @foreach ($pmFields as $pmField)
                            <option value='{{ $pmField->id }}'>{{ $pmField->label }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="sfobject" class="col-sm-3">
                        <strong>Salesforce Object</strong><br />
                        <select id="object" class="form-control">
                            <option value=''>Select</option>
                            @foreach ($sfObjects as $sfObject)
                            <option value='{{ $sfObject->object }}'>{{ $sfObject->object }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <strong>Field</strong><br />
                        <select id="sfField" class="form-control">
                            <option value=''>Select</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <strong>Map</strong><br />
                        <button class="btn btn-default" onclick="map();">Connect</button>
                    </div>
                </div>
                <div class="row">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Portfolio Manager</th>
                                <th>Object</th>
                                <th>Salesforce</th>
                                <th>Un-map</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($related as $match)
                            <tr>
                                <td>{{ $match->pmFields->label }}</td>
                                <td>{{ $match->sfFields->object }}</td>
                                <td>{{ $match->sfFields->label }}</td>
                                <td><i class="fa fa-chain-broken unlink" data-id="{{ $match->id }}" aria-hidden="true"></i></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="salesforce">
                <h4>Salesforce Fields</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Object</th>
                            <th>Field Name</th>
                            <th>Valid</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($sfFields as $field)
                        <tr>
                            <td>{{ $field->object }}</td>
                            <td>{{ $field->label }}</td>
                            <td>
                            @if ($field->status == 1)
                                <i class="fa fa-thumbs-o-up text-success"></i>
                            @else
                                <i class="fa fa-thumbs-o-down text-danger"></i>
                            @endif
                            </td>
                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $field->updated_at)->format('m/d/Y g:i a') }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="pm">
                <h4>PM Fields</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Field Name</th>
                            <th>Valid</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($pmFields as $field)
                        <tr>
                            <td>{{ $field->label }}</td>
                            <td>
                            @if ($field->status == 1)
                                <i class="fa fa-thumbs-o-up text-success"></i>
                            @else
                                <i class="fa fa-thumbs-o-down text-danger"></i>
                            @endif
                            </td>
                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $field->updated_at)->format('m/d/Y g:i a') }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="settings">
                <h4>Pull/Parse field data & schedule</h4>
                <button class="btn btn-default" onclick="pmdata();">Refresh PM Data</button>
                <button class="btn btn-warning" onclick="sfdata();">Refresh Salesforce Data</button>
            </div>
        </div>

    </div>
</div>
@section('scripts')
<script>
function sfdata()
{
    var ans = prompt("Please enter the password for this function", "");
    if (ans != null) {
        if (ans != '') {
            if (ans == '12345') {
                $.ajax({
                    type: 'POST',
                    url: '/fields/sf',
                    data: { _token: $('meta[name="csrf-token"]').attr('content')},
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
        }
    }
}

function pmdata()
{
    var ans = prompt("Please enter the password for this function", "");
    if (ans != null) {
        if (ans != '') {
            if (ans == '12345') {
                $.ajax({
                    type: 'POST',
                    url: '/fields/pm',
                    data: { _token: $('meta[name="csrf-token"]').attr('content')},
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
        }
    }
}

function map()
{
    var pmFieldId = $("#pmFields").val();
    var sfFieldId = $("#sfField").val();
    
    $.ajax({
        type: 'POST',
        url: '/fields/relate',
        data: {_token: $('meta[name="csrf-token"]').attr('content'), sf_field: sfFieldId, pm_field: pmFieldId },
        success: function(response) {
        }
    });
}

$(document).on('click', '.unlink', function() {
    var ans = confirm("are you sure?");
    if (ans) {
        var id = $(this).data('id');
        $.ajax({
            url: '/fields/unrelate/'+id,
            type: 'DELETE',
            data: {_token: $('meta[name="csrf-token"]').attr('content')},
            success: function(response) {
                console.log(response);
            }
        });
    }
});
    
$(document).ready(function() {
    var sfFields = {!! json_encode($sfFields) !!};
    var sfObjects = {!! json_encode($sfObjects) !!};

    $("#object").on('change', function() {
        $('#sfField').children('option:not(:first)').remove();
        var fields = [];
        var object = $(this).val();
        $(sfFields).each(function(idx, value) {
            if (value.object == object) {
                $("#sfField").append($('<option/>', {
                    value: value.id,
                    text: value.label
                }))
            }
        });

        
    });
});
</script>
@stop
@endsection
