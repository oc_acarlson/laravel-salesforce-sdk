@extends('layouts.app')

@section('content')
<div class="container">
    <div class="subtitle">View and manage Fields between PM and Salesforce</div>
    <fieldtabs :sffields="{{ json_encode($sfFields) }}" :pmfields="{{ json_encode($pmFields) }}" :related="{{ json_encode($related) }}" :objects="{{ json_encode($sfObjects) }}"></fieldtabs>
</div>
@endsection
