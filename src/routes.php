<?php
Route::group(['middleware' => ['web', 'auth']], function() {
    Route::get('/fields', 'OverlayConsulting\Salesforcesdk\Controllers\FieldsController@index');
    Route::post('/fields/sf', 'OverlayConsulting\Salesforcesdk\Controllers\FieldsController@reloadSfData');
    Route::post('/fields/pm', 'OverlayConsulting\Salesforcesdk\Controllers\FieldsController@reloadPmData');
    Route::post('/fields/relate', 'OverlayConsulting\Salesforcesdk\Controllers\FieldsController@relateTo');
    Route::delete('/fields/unrelate/{id}', 'OverlayConsulting\Salesforcesdk\Controllers\FieldsController@unrelateTo');
});