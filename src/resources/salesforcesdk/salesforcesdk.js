/**
 * Salesforce SDK package components
 */

Vue.component('fieldtabs', require('./components/fieldtabs.vue'));
Vue.component('field-summary', require('./components/fieldsummary.vue'));
Vue.component('salesforce-fields', require('./components/sffields.vue'));
Vue.component('pm-fields', require('./components/pmfields.vue'));
Vue.component('fields-settings', require('./components/settings.vue'));
Vue.component('field-relation', require('./components/fieldrelations.vue'));

window.Salesforcesdk = new Vue();