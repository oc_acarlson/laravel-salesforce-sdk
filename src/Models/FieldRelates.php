<?php

namespace OverlayConsulting\Salesforcesdk\Models;

use Illuminate\Database\Eloquent\Model;

class FieldRelates extends Model
{
    public function sfFields()
    {
        return $this->belongsTo('OverlayConsulting\Salesforcesdk\Models\Field', 'sf_id', 'id');
    }
    
    public function pmFields()
    {
        return $this->belongsTo('OverlayConsulting\Salesforcesdk\Models\Field', 'pm_id', 'id');
    }
}
