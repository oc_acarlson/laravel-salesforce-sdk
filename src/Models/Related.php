<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Related extends Model
{
    protected $table = 'field_relates';
    
    public function pm()
    {
        return $this->hasMany('OverlayConsulting\Salesforcesdk\Models\Field', 'id', 'pm_id');
    }
    
    public function sf()
    {
        return $this->hasMany('OverlayConsulting\Salesforcesdk\Models\Field', 'id', 'sf_id');
    }
}
