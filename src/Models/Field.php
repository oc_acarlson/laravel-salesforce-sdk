<?php

namespace OverlayConsulting\Salesforcesdk\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public $fillable = ['object', 'label', 'source'];
    
    public function relateSf()
    {
        return $this->hasMany('OverlayConsulting\Salesforcesdk\Models\FieldRelates', 'sf_id', 'id');
    }
    
    public function relatePm()
    {
        return $this->hasMany('OverlayConsulting\Salesforcesdk\Models\FieldRelates', 'pm_id', 'id');
    }
}
