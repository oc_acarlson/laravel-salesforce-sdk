<?php

/**
 * Pulled from the Denver Bridge, will need to be modified for Boulder.
 */
namespace OverlayConsulting\Salesforcesdk\Traits;

/**
 * This trait formats the data for pushing into salesforce.  It does not actually
 * push into salesforce as I want to move away from the custom SF API code and more
 * into the generic SOAP module I'm writing.  I also want the code to be somewhat
 * agnostic, so a switch to REST vs SOAP  would be feasible in the future.
 */
use Carbon\Carbon;
use OverlayConsulting\Salesforcesdk\Models\Related;
use OverlayConsulting\Salesforcesdk\Models\Field;
use OverlayConsulting\Salesforcesdk\Models\FieldRelates;
use OverlayConsulting\Salesforcesdk\Salesforce\Utils\Query;


trait SalesforcePush
{
    public function updateBuilding($submission)
    {
        // setup
        $newData = [];
        $pmData = [];
        $submittedData = json_decode($submission);

        // get a list of related fields by object
        $sfFields = Field::where('source', 'Salesforce')
            ->where('object', 'Building__c')->get();

        foreach ($sfFields as $sfField) {
            if (count($sfField->relateSf) > 0) {
                $pmLabel = Field::find($sfField->relateSf[0]->pm_id);
                $pmData[$pmLabel->label] = $sfField->label;
            }
        }

        foreach ($pmData as $key => $sfKey) {
            if (($submittedData->$key != "0") && (!is_null($submittedData->$key)) && ($submittedData->$key != 'Not Available')) {
                if (preg_match('/_Date_/', $sfKey)) {
                    $newData[$sfKey] = Carbon::parse($submittedData->$key)->tz('America/Denver')->toAtomString();
                } else {
                    $newData[$sfKey] = $submittedData->$key;
                }
            }

        }

        return $newData;
    }
    
    public function updateEnergy($submission)
    {
        $newData = [];  
        $energyData = [];
        $data = json_decode($submission);

        // get a list of related fields by object
        $sfFields = Field::where('source', 'Salesforce')
            ->where('object', 'Energy__c')->get();
        
        foreach ($sfFields as $sfField) {
            if (count($sfField->relateSf) > 0) {
                $energyLabel = Field::find($sfField->relateSf[0]->pm_id);
                $energyData[$energyLabel->label] = $sfField->label;
            }
        }

        foreach ($energyData as $key => $sfKey) {
            if (($data->$key != "0") && (!is_null($data->$key)) && ($data->$key != 'Not Available')) {
                if ((preg_match('/Year/', $sfKey)) || (preg_match('/date/', $key))) {
                    $newData[$sfKey] = date('Y-m-d', strtotime($data->$key));
                } else {
                    if (preg_match('/No/', $data->$key)) {
                        $newData[$sfKey] = 0;
                    } else {
                        $newData[$sfKey] = $data->$key;
                    }
                    
                }
            }
        }
        
        if (isset($newData['Recent_Energy_Star_Certification__c'])) {
            $recentYear = Carbon::parse($newData['Recent_Energy_Star_Certification__c'])->format('Y-m-d');
            $newData['Recent_Energy_Star_Certification__c'] = $recentYear;
            
            $total = explode(", ", $data->energy_star_certification_years_certified);
            $newData['Energy_Star_Years_Certified__c'] = count($total);
        }
        
        foreach ($newData as $key => $value) {
            if (preg_match('/Yes/', $value)) {
                $newData[$key] = 1;
            }
        }

        return $newData;
    }
    
    public function updateAlerts($building)
    {
        $newData = [];
        $alertData = [];
        $data = json_decode($building->submission);
        
        // get a list of related fields by object
        $sfFields = Field::where('source', 'Salesforce')
            ->where('object', 'PM_Alerts__c')->get();
        
        foreach ($sfFields as $sfField) {
            if (count($sfField->relateSf) > 0) {
                $alertLabel = Field::find($sfField->relateSf[0]->pm_id);
                $alertData[$alertLabel->label] = $sfField->label;
            }
        }
        
        foreach ($alertData as $key => $sfKey) {
            if (($data->$key != "0") && (!is_null($data->$key)) && ($data->$key != 'Not Available')) {
                if ($data->$key == 'Ok') {
                    $newData[$sfKey] = 0;
                } else {
                    $newData[$sfKey] = 1;
                }
            }
        }
        
        return $newData;
    }
    
    /**
     * 
     * @param type $building
     * @return string
     */
    public function updateAddresses($building)
    {
        $newData = [];
        $addressData = [];
        $data = json_decode($building->submission);

        // get a list of related fields by object
        $sfFields = Field::where('source', 'Salesforce')
            ->where('object', 'Address__c')->get();

        foreach ($sfFields as $sfField) {
            if (count($sfField->relateSf) > 0) {
                $addrLabel = Field::find($sfField->relateSf[0]->pm_id);
                $addressData[$addrLabel->label] = $sfField->label;
            }
        }
        
        foreach ($addressData as $key => $sfKey) {
            if (($data->$key != "0") && (!is_null($data->$key)) && ($data->$key != 'Not Available')) {
                if (preg_match('/Year/', $sfKey)) {
                    $newData[$sfKey] = date('Y-m-d', strtotime($data->$key));
                } else {
                    if (preg_match('/No/', $data->$key)) {
                        $newData[$sfKey] = 0;
                    } else {
                        $newData[$sfKey] = $data->$key;
                    }
                }
            }
        }
             
        return $newData;
    }
    
    /**
     * we have 2 contacts for every building, so lets be prepared to return 2
     * 
     * @param type $building
     */
    public function updateContacts($building)
    {
        $newData = [];
        $data = json_decode($building->submission);
        $owner = new \stdclass;
        $manager = new \stdclass;
        
        $ownerName = explode(" ", $data->on_behalf_of);
        $owner->FirstName = array_shift($ownerName); // should remove the 1st entry
        $owner->LastName = implode(" ", $ownerName); // concatenate all remaining values
        $owner->Email = $data->email;
        $owner->Phone = $data->phone;
        $owner->Contact_Source__c = 'Portfolio Manager';
        $owner->Contact_Type__c = 'Portfolio Manager';
        
        $managerName = explode(" ", $data->propertys_portfolio_manager_account_holder);
        $manager->FirstName = array_shift($managerName);
        $manager->LastName = implode(" ", $managerName);
        $manager->Email = $data->propertys_portfolio_manager_account_holder_email;
        $manager->Contact_Source__c = 'Portfolio Manager';
        $manager->Contact_Type__c = 'Portfolio Manager';
        
        return [$owner, $manager];
    }
    
    /**
     * Query salesforce for the parent object, and all child object ids
     * and set them as object params
     *
     * @param type $submission
     */
    public function querySfData($dbid)
    {
        $sfQuery = new Query;
        
        $queryStr = "SELECT Id, Name, Building_Status__c, (SELECT Id FROM Energy_Usage__r ), (SELECT Id FROM PM_Alerts__r), (SELECT Id FROM addresses__r WHERE Address_Type__c = 'Building' AND Address_Source__c = 'Portfolio Manager'), (SELECT Id FROM Submissions__r) FROM building__c WHERE Name = '" . $dbid . "'";
        $result = $sfQuery->query($queryStr);
        $sfComponentIds = [];
        
        $sfComponentIds['building'] = ['Id' => $result[0]->Id, 'status' => $result[0]->Building_Status__c];
        if (isset($result[0]->Alerts__r)) {
            $sfComponentIds['alerts'] = $result[0]->PM_Alerts__r->records[0]->Id;
        } else {
            $sfComponentIds['alerts'] = '';
        }

        if (isset($result[0]->Energy_Usage__r)) {
            $sfComponentIds['energy'] = $result[0]->Energy_Usage__r->records->Id;
        } else {
            $sfComponentIds['energy'] = '';
        }

        if (isset($result[0]->Addresses__r)) {
            foreach ($result[0]->Addresses__r->records as $record) {
                $sfComponentIds['addresses'][] = $record;
            }
        } else {
            $sfComponentIds['addresses'] = '';
        }

        if (isset($result[0]->Submissions__r)) {
            $sfComponentIds['submissions'] = $result[0]->Submissions__r->records->Id;
        } else {
            $sfComponentIds['submissions'] = '';
        }

        return $sfComponentIds;
    }
}
