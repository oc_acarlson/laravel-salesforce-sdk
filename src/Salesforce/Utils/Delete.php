<?php
/**
 * delete a record
 */

namespace OverlayConsulting\Salesforcesdk\Salesforce\Utils;

use OverlayConsulting\Salesforcesdk\Helpers\SFLogger;

/**
 *
 */
class Delete extends Salesforce
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Delete objects/rows by Salesforce Id
     * 
     * @param type $ids
     * @return type
     */
    public function deleteById($ids)
    {
        $response = $this->client->delete($ids);
        SFLogger::log($response);
        return $response;
    }
}
