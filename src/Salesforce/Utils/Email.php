<?php
/**
 *
 */

namespace OverlayConsulting\Salesforcesdk\Salesforce\Utils;

use OverlayConsulting\Salesforcesdk\Salesforce\SDK\SingleEmailMessage;
use OverlayConsulting\Salesforcesdk\Helpers\SFLogger;

/**
 *
 */
class Email extends Salesforce
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Send a single text email via salesforce API & bound to object activity
     * $mailTo, $replyTo, $message, and $subject are all required parameters.
     * $priority will assign a mail priority where applicable.
     * $saveActivity true/false will attempt to save the mail being sent
     * as an activity tied to the object.  The object must have activity/history enabled
     * and you need 
     * 
     * @param String or Array $mailTo
     * @param String $message - the text message of the email
     * @param String $subject - the subject of the email
     * @param String $replyTo - the from email address
     * @param String $objectId - The salesforce Id you wish to tie the email to
     * @param String $priority - the email priority Low, Medium, High
     * @param Boolean $saveActivity - true/false - save this email as an activity 
     * @return object
     */
    public function singleMail($mailTo, $message, $subject, $replyTo, $objectId, $priority='Low', $saveActivity=true)
    {
        $mail = new SingleEMailMessage();
        $mail->setToAddresses($mailTo);
        $mail->setBccSender(true);
        $mail->setBccAddresses([config('EBCHelp@Hennepin.us')]);
        $mail->setPlainTextBody($message);
        $mail->setSaveAsActivity($saveActivity);
        if ($saveActivity) {
            $mail->setWhatId($objectId);
        }
        $mail->setEmailPriority($priority);
        $mail->setSubject($subject);
        $mail->setReplyTo($replyTo);
        $response = $this->client->sendSingleEmail([$mail]);
        return $response;
    }
}
