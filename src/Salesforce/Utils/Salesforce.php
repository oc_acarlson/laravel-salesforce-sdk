<?php
/**
 * Abstract class for high level object definition and common functions
 */

namespace OverlayConsulting\Salesforcesdk\Salesforce\Utils;
use OverlayConsulting\Salesforcesdk\Salesforce\SDK\SforceEnterpriseClient;
use OverlayConsulting\Salesforcesdk\Helpers\SFLogger;

/**
 * Abstract class Salesforce
 */
abstract class Salesforce
{

    public $client;
    private $credentials; // this is the combined token+password in the correct order

    public function __construct()
    {
        // always log in?
        if (!$this->client) {
            $this->login();
        }
    }

    /**
     * log in to salesforce soap API
     * 
     * @return type
     */
    public function login()
    {
        $wsdlPath = storage_path() . config('salesforce.WSDL');
        $this->client = new SforceEnterpriseClient();
        $this->client->createConnection($wsdlPath);
        $pw = config('salesforce.PASSWORD');
        $tk = config('salesforce.TOKEN');
        $credentials = $pw.$tk;
        $this->client->login(config('salesforce.USERNAME'), $credentials);
        return;
    }
}
