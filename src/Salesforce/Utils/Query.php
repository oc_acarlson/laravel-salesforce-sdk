<?php
/**
 * Find a record or object
 */

namespace OverlayConsulting\Salesforcesdk\Salesforce\Utils;

use OverlayConsulting\Salesforcesdk\Salesforce\Utils\Salesforce;
use OverlayConsulting\Salesforcesdk\Helpers\SFLogger;

/**
 *
 */
class Query extends Salesforce
{

    public function __construct()
    {
        parent::__construct();
    }

    public function query($query) {
        $response = $this->client->query($query);
        SFLogger::log($response);
        return $response->records;
    }

	public function fullQuery($query) {
        $response = $this->client->query($query);
        SFLogger::log($response);
        return $response;
    }
}
