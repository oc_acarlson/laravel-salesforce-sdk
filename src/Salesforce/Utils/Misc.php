<?php
/**
 * Find a record or object
 */

namespace OverlayConsulting\Salesforcesdk\Salesforce\Utils;

use OverlayConsulting\Salesforcesdk\Salesforce\Utils\Salesforce;
use \Log;

/**
 *
 */
class Misc extends Salesforce
{

    public function __construct()
    {
        parent::__construct();
    }

    public function sObject($object)
    {
        $response = $this->client->describeSObject($object);
        return $response;
    }

    public function describeSF()
    {
        return $this->client->describeGlobal();
    }
}