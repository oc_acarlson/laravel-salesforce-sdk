<?php
/**
 *
 */

namespace OverlayConsulting\Salesforcesdk\Salesforce\Utils;

use OverlayConsulting\Salesforcesdk\Helpers\SFLogger;

/**
 *
 */
class Create extends Salesforce
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * insert a single object
     * 
     * @param object or array $record
     * @param string $sfObject
     * @return type
     */
    public function newRecord($record, $sfObject)
    {
        $this->result = $this->client->create([$record], $sfObject);
        SFLogger::log($this->result);
        return $this->result;
    }

    /**
     * Insert multiple objects
     * 
     * @param type $records
     */
    public function newRecords($records)
    {
        $this->result = $this->client->create($records, $sfObject);
        SFLogger::log($this->result);
        return $this->result;
    }
}
