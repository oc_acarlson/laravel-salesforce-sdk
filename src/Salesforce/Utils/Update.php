<?php
/**
 *
 */

namespace OverlayConsulting\Salesforcesdk\Salesforce\Utils;

use OverlayConsulting\Salesforcesdk\Helpers\SFLogger;

/**
 *
 */
class Update extends Salesforce
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * update a single record in salesforce
     *
     * @param object/array $objects - the data to update
     * @param string $sfObject - the salesforce object (contact, account, custom)
     */
    public function updateRecords($objects, $sfObject)
    {
        $this->result = $this->client->update($objects, $sfObject);
        SFLogger::log($this->result);
        return $this->result;
    }

    /**
     * Do an Upsert - insert on new, update on existing
     * Does not work great with Contacts keyed on email
     * 
     * @param array $objects - data being updated/inserted
     * @param string $sfObject - the object being updated (contact, account, custom)
     * @param string $key - the key/foreign key to find records
     * @return object
     */
    public function upsertRecords($objects, $sfObject, $key)
    {
        $this->result = $this->client->upsert($key, [$objects], $sfObject);
        SFLogger::log($this->result);
        return $this->result;
    }
}
