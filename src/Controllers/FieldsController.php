<?php

namespace OverlayConsulting\Salesforcesdk\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use OverlayConsulting\Salesforcesdk\Models\Field;
use OverlayConsulting\Salesforcesdk\Models\FieldRelates;
use OverlayConsulting\Salesforcesdk\Salesforce\Utils\Misc;

class FieldsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * rewrite this so it doesn't pull every time, only on command
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $related = FieldRelates::with(['sfFields', 'pmFields'])->get();
        $sfFields = Field::where('source', 'Salesforce')->get();
        $pmFields = Field::where('source', 'Portfolio Manager')->get();
        $sfObjects = Field::whereNotNull('object')->select('object')->distinct()
            ->orderBy('object')->get();

        return view('Salesforcesdk::fields.index')->with('related', $related)
            ->with('sfFields', $sfFields)->with('pmFields', $pmFields)
            ->with('sfObjects', $sfObjects);
    }

    /**
     * set the relationship between 2 fields
     * 
     * @param Request $request
     */
    public function relateTo(Request $request)
    {
        $relates = new FieldRelates;
        $relates->sf_id = $request->sf_field;
        $relates->pm_id = $request->pm_field;
        $relates->save();
    }
    
    /**
     * remove the relationship between 2 fields
     * 
     * @param Request $request
     */
    public function unrelateTo($id)
    {
        FieldRelates::find($id)->delete();
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // this needs a ton of validation/verification
        // create the new field
        $field = new Field;
        $field->source = $request->source;
        $field->object = $request->object;
        $field->label = $request->label;
        $field->translates_to = $request->translates_to;
        $field->save();

        // relate the original field to the new one
        $oField = Field::find($request->translates_to);
        $oField->translates_to = $field->id;
        $oField->save();
    }

    public function reloadSfData()
    {
        $data = new Misc;
        $result = $data->describeSF();
        $sfFields = [];
        $source = "Salesforce";

        foreach ($result->sobjects as $object) {
            if (preg_match('/__c$/', $object->name)) {
                $objects[$object->label] = $object->name;
                $sobject = $data->sObject($object->name);
                $fieldData = []; // flush the data object before filling it
                foreach ($sobject->fields as $field) {
                    $field = Field::updateOrCreate(['source' => $source, 'object' => $object->name, 'label' => $field->name]);
                    $field->object = $object->name;
                    $field->status = 1;
                    $field->save();
                }
            }
        }
        
        // get Contacts fields
        if (config('salesforce.USE_SF_CONTACTS')) {
            $contactData = $data->sObject('Contact');
            foreach ($contactData->fields as $contactField) {
                $field = Field::updateOrCreate(['source' => $source, 'label' => $contactField->name]);
                $field->object = 'Contact';
                $field->save();
            }
        }
    }

    /**
     * this really shouldn't be in here.
     */
    public function reloadPmData()
    {
        $path = storage_path() . "/app/uploads";
        $files = array_diff(scandir($path), ['.', '..', 'parse.php']);
        $lastFile = $path . "/" . end($files);
        $xml = simplexml_load_file($lastFile);
        $data = $xml->informationAndMetrics->row;

        $pmFields = (array) $data[0];
        $pmFields = array_keys($pmFields);

        foreach ($pmFields as $field) {
            $pmField = Field::updateOrCreate(['source' => 'Portfolio Manager', 'label' => $field]);
            $pmField->status = 1;
            $pmField->save();
        }
    }
}
