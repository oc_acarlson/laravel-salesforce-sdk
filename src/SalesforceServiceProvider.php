<?php

namespace OverlayConsulting\Salesforcesdk;

use Illuminate\Support\ServiceProvider;

class SalesforceServiceProvider extends ServiceProvider
{
    // define artisan commands
    protected $commands = [
        'OverlayConsulting\Salesforcesdk\Commands\FieldCheck'
    ];
    
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/salesforce.php' => config_path('salesforce.php'),
            __DIR__ . '/Views/fields' => resource_path('views/vendor/salesforcesdk/fields'),
            __DIR__ . '/resources/salesforcesdk' => resource_path('assets/vendor/salesforcesdk'),
        ]);

        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->loadMigrationsFrom(__DIR__.'/Migrations');

        $this->loadViewsFrom(__DIR__.'/Views', 'Salesforcesdk');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // setup artisan commands
        $this->commands($this->commands);
        
        // setup logging class
        $this->app->singleton(SFLogger::class, function($app) {
            return new SFLogger($app);
        });
    }
}
