# README #
This package converts the popular Salesforce SDK for PHP into PSR compliant, Autoloading 
code capable of running on the popular Laravel PHP Framework.  The point/goal of this
package is to enable salesforce API commands in popular frameworks like Laravel. 


### How do I get set up? ###
composer require overlayconsulting/salesforcesdk

Edit .env and add your salesforce credentails.  DO NOT combine the password
and token.  The application will do that for you.

```
SALESFORCE_API_USERNAME=me@example.com
SALESFORCE_API_PASSWORD=secret
SALESFORCE_API_TOKEN=1234567890abcdefghijklm
```

Publish the config, views, and components
This package now uses Vue components for managing/relating fields between PM and Salesforce. This makes 
it easier to manage what fields get mapped between the two services.  


```
php artisan vendor:publish --tag="overlay"
```
You will need to modify the config file in config/salesforce.php for your salesforce environment. 
``` LOGIN_API ```  your login url
``` WSDL ```  You will need to download either your enterprise or partner WSDL file and save it.  The 
preferred location is in storage/app.  Edit this line to include the filename as well as location.  I would
not put this file in a publicly accessible location.
Anytime you change objects in Salesforce, you need to download a new WSDL.

Publishing also puts blade views in resources/views/vendor/salesforcesdk and
vue components in resources/assets/vendor/salesforcesdk.  You can customize the
views/vues as you desire.  These examples are based on the Bulma.io css UI framework.

There are 2 blade files in resources/views/vendor/salesforcesdk, a pure blade/jquery based option
and a vue.js option that depends on Bulma.io and Vue.js and uses no jQuery.

To use the Vue components, point your view in your controller to the index_vue.blade.php or rename it to index.blade.php.
Add this line to your app.js in resources/assets/js/app.js
```
require('../vendor/salesforcesdk/salesforcesdk.js');
```
and run npm run dev to compile the assets.



### Usage ###

Common Salesforce commands are broken out into individual class functions for easier use.
Include the function just as you would any other file.  Salesforce API login occurs as part of the
class inheritance.

Example - Query
``` 
use OverlayConsulting\Salesforcesdk\Salesforce\Utils\Query;
...
... inside your class/method you can call the API 
...
$salesforce = new Query;
$queryString = "SELECT Id, FirstName, LastName FROM Contact";
$response = $salesforce->query($queryString);
dd($response);
```


Example - Insert/Create
```
use OverlayConsulting\Salesforcesdk\Salesforce\Utils\Create;
use \StdClass;
...
...
...
// connect to the salesforce api
$salesforce = new Create;

// prepare your object or array to insert
$contact = new StdClass;
$contact->FirstName = 'Bob';
$contact->LastName = 'Smith';
$contact->Email = 'b.smith@example.com';

// insert the new object into salesforce
$result = $salesforce->create([$contact], 'Contact'); 

// The data you wish to insert must be nested in an array
// You must specify the Salesforce Object you wish to insert to as well, referenced by the API Name.
```

Example - Delete
```
use OverlayConsulting\Salesforcesdk\Salesforce\Utils\Delete;
...
...
...
$salesforce = new Delete;
$idsToDelete = ['ab234s34sdfs3', 'asdfis89sdf98sdf', 'asdf0s0s0s8s0', 'as8s9s89asd98s'];
$response = $salesforce->delete($idsToDelete);
dd($response);
```

Example - Email
```
```

Example - Update/Upsert
```
```
### Console Commands ###
There are 4 core console commands to handle processing PM data request submissions
getting salesforce status of each building's submission, and submitting each
submission into salesforce.  The fourth file deals with the initial data import 
into salesforce from the assessors data.  These files can be run directly or copied
into the app/Console/Commands folder and modified to meet the clients needs.


### Logging ###
A new logging helper has been added to write all salesforce responses to a 
separate log file in /storage/logs/salesforce.log.  This does not use built in
laravel logging or stack dumps, but simple file_put_contents with FILE_APPEND.
The log format is generally date/time, message, and data.  This is helpful in
determining if a salesforce insert/update/delete fails.  Logging can be controlled
by config/salesforce, and .env keys SF_LOGGING and SF_LOG_PATH, which enables/disables
output logging and defines the log filename.  Default is off.

### Traits ###
There is a PHP Trait that is core to the code now.  Because both CLI/Cron and web
users can push data into salesforce, a trait is better for a single library.  The
trait takes the PM data and converts the keys from the PM data file into Salesforce
field keys.  It then compares the data for the building in Salesforce with the 
data in the PM data file/submission.  Any variance set's that building's status
as 'to sync'.  This allows the system to determine what building submissions have
newer data.  The submit process limits which buildings are synced by status in
salesforce.  This is provided more as an example around a specific use case.
It illustrates using the related field as a map between PM and SF fields as well
as comparing data instead of just forcing data into SF regardless.


How to use the API is better suited to the Salesforce PHP Examples via
https://developer.salesforce.com/page/PHP_Toolkit_20.0_Samples.

### TODO / Wishlist ###
* Implement Facades for quick/easy access 
* Implement Events for tracking SF activity
* Implement Login PW/Token Flip-flop testing
* Implement Unit Tests
* Implement More SF API Commands
* Implement manual/DIY commands
* consider Service Container implementation?


### disclaimer ###
We are not affiliated with salesforce in anyway.  We used/downloaded the salesforce PHP sdk
and converted it to PSR-4 namespace standards for use in our own projects and decided that
others might find it useful.  
This software comes with no warranty, implied or assumed.


